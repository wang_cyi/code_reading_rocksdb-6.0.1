//  Copyright (c) 2011-present, Facebook, Inc.  All rights reserved.
//  This source code is licensed under both the GPLv2 (found in the
//  COPYING file in the root directory) and Apache 2.0 License
//  (found in the LICENSE.Apache file in the root directory).

#pragma once

#include <condition_variable>
#include <mutex>
#include <queue>
#include <utility>

namespace rocksdb {

template <class T>
class channel {
 public:
  explicit channel() : eof_(false) {}

  channel(const channel&) = delete;
  void operator=(const channel&) = delete;

  void sendEof() {
    std::lock_guard<std::mutex> lk(lock_);
    eof_ = true;
    cv_.notify_all();
  }

  bool eof() {
    std::lock_guard<std::mutex> lk(lock_);
    return buffer_.empty() && eof_;
  }

  size_t size() const {
    std::lock_guard<std::mutex> lk(lock_);
    return buffer_.size();
  }

  // writes elem to the queue
  //小王疑问1：参数为T&&,不处理区分 左右值吗？
  //小王疑问2：read不这样写
  void write(T&& elem) {
    std::unique_lock<std::mutex> lk(lock_);
    //WHY not buffer_.emplace(elem)
    buffer_.emplace(std::forward<T>(elem));
    cv_.notify_one();
     //1 引用 2 重载
  }
  //C++中push_back和emplace_back的区别
  //https://zhuanlan.zhihu.com/p/213853588
  //https://en.cppreference.com/w/cpp/utility/forward

  // Item 25: Use std::move on rvalue references,
//std::forward on universal references.
  /// Moves a dequeued element onto elem, blocking until an element
  /// is available.
  // returns false if EOF
  bool read(T& elem) {
    std::unique_lock<std::mutex> lk(lock_);
    cv_.wait(lk, [&] { return eof_ || !buffer_.empty(); });
    //小王疑问：为什么不阻塞住呢
    if (eof_ && buffer_.empty()) {
      return false;
    }
    //小王疑问：为什么符合语法操作
    elem = std::move(buffer_.front());
    buffer_.pop();
    cv_.notify_one();
    return true;
  }

 private:
  std::condition_variable cv_;
  std::mutex lock_;
  std::queue<T> buffer_;
  //先进先出。出队的一头是队头，入队的一头是队尾
  bool eof_;
};
}  // namespace rocksdb

//https://www.cplusplus.com/reference/utility/move/?kw=move
//Returns an rvalue reference to arg.
